package com.myProject.sms.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.myProject.sms.entity.Student;
import com.myProject.sms.repository.StudentRepository;

@Controller
public class StudentController {
	@Autowired
	StudentRepository repo;
	
	@RequestMapping("students")
	public ModelAndView listStudents() {
		ModelAndView mav = new ModelAndView("students");
		mav.addObject("students1",repo.findAll());
		return mav;
		
	}
	@RequestMapping("/students/new")
	public ModelAndView addNewStudent(Student stud) {
		//System.out.println("helooooo");
		ModelAndView mav = new ModelAndView("CreateStudent");
		mav.addObject("student",stud);
		return mav;
	}
	@PostMapping("/students")
	public String saveStudents(Student stud) {
		repo.save(stud);
		return "redirect:/students";
	}
	@RequestMapping("/students/edit/{id}")
	public ModelAndView editStudent(@PathVariable long id) {
		ModelAndView mav = new ModelAndView("editStudent");
		Student stud = repo.findById(id).get();
		mav.addObject("student",stud);
		return mav;
	}
	
	@PostMapping("/students/{id}")
	public String updateStudent(Student stud) {
		repo.save(stud);
		return "redirect:/students";
	
	}
	
	@RequestMapping("/students/delete/{id}")
	public String deleteStudent(@PathVariable long id) {
		repo.deleteById(id);
		return "redirect:/students";
		
	}

}

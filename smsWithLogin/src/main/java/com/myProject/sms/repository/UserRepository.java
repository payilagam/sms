package com.myProject.sms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.myProject.sms.entity.RegistrationForm;

import jakarta.transaction.Transactional;

public interface UserRepository extends JpaRepository<RegistrationForm, Long>{
	
	
		
		@Query("SELECT rf FROM RegistrationForm rf WHERE rf.username = ?1")
		RegistrationForm findByUsername(String username);

}

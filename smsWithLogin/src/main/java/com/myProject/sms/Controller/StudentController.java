package com.myProject.sms.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.myProject.sms.entity.RegistrationForm;
import com.myProject.sms.entity.Student;
import com.myProject.sms.repository.StudentRepository;
import com.myProject.sms.repository.UserRepository;

@Controller
public class StudentController {

	private String flag;
	private final StudentRepository repo;
    private final UserRepository urepo;

    @Autowired
    public StudentController(StudentRepository repo, UserRepository urepo) {
        this.repo = repo;
        this.urepo = urepo;
    }
	@RequestMapping("login")
	public ModelAndView login(RegistrationForm rform) {
		flag="Red";
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("regiform",rform);
		return mav;
		
	}
	
	@PostMapping("/login")
	public String checkUsername(RegistrationForm rform) {
		try {
		RegistrationForm rrform = urepo.findByUsername(rform.getUsername());
		if(rrform.getPassword().equals(rform.getPassword())) {
			flag="Grean";
			return "redirect:/students?success";
		}
		else {
			flag="Red";
			return "redirect:/login?failed";
		}
		}catch(NullPointerException exp) {
			System.out.println("not valid");
			flag="Red";
			return "redirect:/login?failed";
		}
	}
	
	@RequestMapping("/register")
	public ModelAndView register(RegistrationForm rform) {
		ModelAndView mav = new ModelAndView("registration");
		mav.addObject("regiform",rform);
		return mav;
	}
	
	@PostMapping("/register")
	public String saveUser(RegistrationForm rform) {
		urepo.save(rform);
		return "redirect:/register?success";
	}
	
	@RequestMapping("students")
	public ModelAndView listStudents() {
		ModelAndView mav = new ModelAndView();
		if(flag=="Grean") {
		mav = new ModelAndView("students");
		mav.addObject("students1",repo.findAll());
		return mav;
		}else {
			mav = new ModelAndView("redirect:/login");
		return mav;
		}
	}
	
	@RequestMapping("/students/new")
	public ModelAndView addNewStudent(Student stud) {
		ModelAndView mav = new ModelAndView("CreateStudent");
		mav.addObject("student",stud);
		return mav;
	}
	@PostMapping("/students")
	public String saveStudents(Student stud) {
		repo.save(stud);
		return "redirect:/students";
	}
	@RequestMapping("/students/edit/{id}")
	public ModelAndView editStudent(@PathVariable long id) {
		ModelAndView mav = new ModelAndView("editStudent");
		Student stud = repo.findById(id).get();
		mav.addObject("student",stud);
		return mav;
	}
	
	@PostMapping("/students/{id}")
	public String updateStudent(Student stud) {
		repo.save(stud);
		return "redirect:/students";
	
	}
	
	@RequestMapping("/students/delete/{id}")
	public String deleteStudent(@PathVariable long id) {
		repo.deleteById(id);
		return "redirect:/students";
	}

}
